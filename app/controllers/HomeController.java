package controllers;

import play.api.Environment;
import play.mvc.*;
import play.data.*;
import play.db.ebean.Transactional;

import java.util.ArrayList;
import java.util.List;
import javax.inject.Inject;

import play.mvc.Http.*;
import play.mvc.Http.MultipartFormData.FilePart;
import java.io.File;

import org.im4java.core.ConvertCmd;
import org.im4java.core.IMOperation;


import views.html.*;

import models.*;
import models.users.*;


public class HomeController extends Controller {

    // Declare a private FormFactory instance
    private FormFactory formFactory;

    private Environment env;

    //  Inject an instance of FormFactory it into the controller via its constructor
    @Inject
    public HomeController(FormFactory f, Environment e) {
        this.env = e;
        this.formFactory = f;
    }

    public Result index() {
        List<Category> categoriesList = Category.findAll();
        return ok(index.render(categoriesList, getUserFromSession()));
    }

    public Result products(Long cat) {

        List<Category> categoriesList = Category.findAll();
        List<Product> productsList = new ArrayList<Product>();

        if(cat == 0) {
       
          productsList = Product.findAll();
        }
        else {

          productsList = Category.find.ref(cat).getProducts();
        }

        return ok(products.render(productsList, categoriesList, getUserFromSession(), env));
    }
    
    @Security.Authenticated(Secured.class)
    @With(AuthAdmin.class)
    public Result addProduct() {

      Form<Product> addProductForm = formFactory.form(Product.class);

      return ok(addProduct.render(addProductForm, getUserFromSession()));
    }

    @Security.Authenticated(Secured.class)
    @With(AuthAdmin.class)
    @Transactional
    public Result addProductSubmit(){

      String saveImageMsg;

      Form<Product> newProductForm = formFactory.form(Product.class).bindFromRequest();

      if(newProductForm.hasErrors()) {

          return badRequest(addProduct.render(newProductForm, getUserFromSession()));
      }

      Product p = newProductForm.get();
      
      if(p.getId() == null) {
      
         p.save();
      }
      else if (p.getId() !=null) {
         p.update();
      }
       
      MultipartFormData data = request().body().asMultipartFormData();
      FilePart image = data.getFile("upload");

      saveImageMsg = saveFile(p.getId(), image);

      flash("success", "Product " + p.getName() + " has been created or updated "+saveImageMsg);

      return redirect(controllers.routes.HomeController.products(0));

    }
    @Security.Authenticated(Secured.class)
    @With(AuthAdmin.class)
    @Transactional
    public Result deleteProduct(Long id) {

      Product.find.ref(id).delete();

      flash("success", "Product has been deleted");

      return redirect(routes.HomeController.products(0));
    }

    public Result contact() {
        List<Category> categoriesList = Category.findAll();
        return ok(contact.render(categoriesList, getUserFromSession()));
    }

    public Result services() {
        List<Category> categoriesList = Category.findAll();
        return ok(services.render(categoriesList, getUserFromSession()));
    }

    public Result tips() {
        List<Category> categoriesList = Category.findAll();
        return ok(tips.render(categoriesList, getUserFromSession()));
    }

 

    @Security.Authenticated(Secured.class)
    @With(AuthAdmin.class)
    @Transactional
    public Result updateProduct(Long id) {

      Product p;
      Form<Product> productForm;

      try {
          p = Product.find.byId(id);

          productForm = formFactory.form(Product.class).fill(p);

          } catch (Exception ex) {
     
            return badRequest("error");
       }

       return ok(addProduct.render(productForm, getUserFromSession()));
     }

    private User getUserFromSession() {

      return User.getUserById(session().get("email"));

    }

    public String saveFile(Long id, FilePart<File> image) {
        if (image != null) {
            // Get mimetype from image
            String mimeType = image.getContentType();
            // Check if uploaded file is an image
            if (mimeType.startsWith("image/")) {
                // Create file from uploaded image
                File file = image.getFile();
                // create ImageMagick command instance
                ConvertCmd cmd = new ConvertCmd();
                // create the operation, add images and operators/options
                IMOperation op = new IMOperation();
                // Get the uploaded image file
                op.addImage(file.getAbsolutePath());
                // Resize using height and width constraints
                op.resize(300,200);
                // Save the  image
                op.addImage("public/images/productImages/" + id + ".jpg");
                // thumbnail
                IMOperation thumb = new IMOperation();
                // Get the uploaded image file
                thumb.addImage(file.getAbsolutePath());
                thumb.thumbnail(60);
                // Save the  image
                thumb.addImage("public/images/productImages/thumbnails/" + id + ".jpg");
                // execute the operation
                try{
                    cmd.run(op);
                    cmd.run(thumb);
                }
                catch(Exception e){
                    e.printStackTrace();
                }
                return " with image file.";
            }
        }
        return " without image file.";
    }

    


}


