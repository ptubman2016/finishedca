# --- Sample dataset

# --- !Ups

insert into category (id,name) values ( 1,'Cats' );
insert into category (id,name) values ( 2,'Dogs' );
insert into category (id,name) values ( 3,'Small animals' );
insert into category (id,name) values ( 4,'Miscellaneous' );



insert into product (id,category_id,name,description,stock,price) values ( 1,3,'Kettle','Steel Electric Kettle',100,55.00 );
insert into product (id,category_id,name,description,stock,price) values ( 2,2,'Fridge freezer','Fridge + freezer large',45,799.00 );
insert into product (id,category_id,name,description,stock,price) values ( 3,1,'Portable Music Player','250GB music player (MP3,MP4,WMA,WAV)',5,99.00 );
insert into product (id,category_id,name,description,stock,price) values ( 4,3,'13inch Laptop','HP laptop,8GB RAM,250GB SSD',45,799.00 );
insert into product (id,category_id,name,description,stock,price) values ( 5,3,'8inch Tablet','Android 5.1 Tablet,32GB storage,8inch screen',5,99.00 );
insert into product (id,category_id,name,description,stock,price) values ( 6,2,'60inch TV','Sony 4K,OLED,Smart TV',12,5799.00 );
insert into product (id,category_id,name,description,stock,price) values ( 7,2,'Washing Machine','1600rpm spin,A+++ rated,10KG',50,699.00 );
insert into product (id,category_id,name,description,stock,price) values ( 8,1,'Phone','Windows 10,5.2inch OLED,3GB RAM,64GB Storage',45,799.00 );
insert into product (id,category_id,name,description,stock,price) values ( 9,1,'10inch Tablet','Windows 10,128GB storage,8inch screen',5,299.00 );
insert into product (id,category_id,name,description,stock,price) values ( 10,3,'Oven','Oven + Grill,Stainless Steel',10,399.00 );
insert into product (id,category_id,name,description,stock,price) values ( 11,1,'Bed','Super King size,super comfort mattress',5,899.00 );
insert into product (id,category_id,name,description,stock,price) values ( 12,2,'Learning JavaScript','Become a JavaScript expert in 2 hours!',50,29.00 );

insert into user (email,name,password,role) values ( 'admin@products.com', 'Alice Admin', 'password', 'admin' );
insert into user (email,name,password,role) values ( 'ptubman2016@gmail.com', 'Philip Tubman', 'password', 'admin' );
insert into user (email,name,password,role) values ( 'aoifeskelly14@hotmail.com', 'Aoife Skelly', 'password', 'admin' );
insert into user (email,name,password,role) values ( 'manager@products.com', 'Bob Manager', 'password', 'manager' );
insert into user (email,name,password,role) values ( 'customer@products.com', 'Charlie Customer', 'password', 'customer' );
